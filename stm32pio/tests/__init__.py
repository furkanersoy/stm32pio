"""
Some unit-tests for stm32pio. Uses sample project to generate and build it. It's OK to get errors on `test_run_editor`
one because you don't necessarily should have all of the editors. Run as

    python3 -m unittest discover -v

or

    python3 -m stm32pio.tests.test -v

(from repo's root)
"""
